var mongoose = require('mongoose');

/* Mongodb serving module
 * Uses Mongoose
 * Here, _id is used with gcm registration ID
 */

var userSchema = mongoose.Schema({
	_id  : String,
    email: String,
	token : String,
    hashed_password : String,
    salt : String,
    temp_str : String,
	monitored_by : String,
	fence : {
		fence_name : String,
		monitored_on : Boolean,
		latitude : Number,
		longitude : Number,
		radius : Number
	},
	monitoring : [{
		_id : String,
		request_status : Boolean
	}]
});

mongoose.connect('mongodb://localhost:27017/data', function(err, db) {
     if(!err) { 
         console.log("MongoDb connected");
      }
});

module.exports = mongoose.model('users', userSchema);