var gcm = require('node-gcm');
var apiKey = "AIzaSyCUu4OrYUrZCxzJBCXk4goDQPfBqBVt1zw";
var sender = new gcm.Sender(apiKey);
var mongoose = require('mongoose');
var user = require('../model/models');

/* This module is serving API for GCM notification to devices
 * It takes GCM id, and message to forward to 
 */
exports.notify = function( gcm_id, title, callback){
	var message = new gcm.Message({
		collapseKey : 'gcm',
		priority : 'high',
		contentAvailable : true,
		delayWhileIdle : true,
		timeToLive : 13,
		data : {
			'title': title,
			'message': 'Geofence Alert',
			'is_update': "NO",
			'msgcnt':'1'
		},
		notification: {
			title : title,
			body: 'Geofence Alert'
		}
	});
	
	/* Fetching from MongoDb
	 */
	var registrationDevs = [];
	var registrationIds = [];
	
	user.findOne({_id: gcm_id}, function(err, sender_doc){
		var sender_cnt = sender_doc.length;
			
		if(sender_cnt == 0) {
			callback({'response': "Device not found"},{'res': false});
		} else {
			//registrationDevs = sender.monitoring
			//for (dev in registrationDevs) {
			//	registrationIds.push(dev._id)
			//}
			var reg_id = sender_doc.monitored_by;
			
			registrationIds.push(reg_id);
			console.log("Checking "+registrationIds);
			
		}
		
	});

	/*
	registrationIds.push('APA91bGvMQ2WKXyI0DUnzTv0z2rYxsPjcwx-UU8pcSoOE256wfiRRcAZvur_otQciILYnQwj6Pdqbzoor9SMiflyLjw8eVSgdWLyk8vvTM2e0dtknR82grrPdkqKIgXLKJAI5ciOyY9WRBvRfZo2IKJiu_34XoEzqQ');
	registrationIds.push('APA91bE-JALeni13h5vmoeIZMSDsMTU8rAWnL3zg8rTKWpSKgSmwFmfM7TUiZ2_rZKIadnXItLz6E2cTeQj4dYRVVoivVsi3MOCtUXXU62GKHCPJSbZKSmOzaU0CZx5qaBSVHREIcQNrGu_py1s9ucE3owgSSlm9Gg');
	*/

	sender.send(message, registrationIds, 3, function(err, result) {
		console.log("Checking while forwarding the geofence violation regId=" +registrationIds);
			
		if(err) console.error(err);
		else console.log(result);
	});
	callback({'response':"Success", 'res':true });
}