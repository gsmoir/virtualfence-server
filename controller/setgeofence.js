var gcm = require('node-gcm');
var mongoose = require('mongoose');
var user = require('../model/models');
var apiKey = "AIzaSyCUu4OrYUrZCxzJBCXk4goDQPfBqBVt1zw";
var sender = new gcm.Sender(apiKey);

exports.setgeofence= function(gcm_id, email, fence_name, latitude, longitude, radius, callback){
	console.log("1. Sent="+email+fence_name+latitude+longitude+radius +gcm_id);

	var gcm_id_of_cared;
	var gcmIds = [];
	
	//Updating fence details details, by finding the cared and also sending notification
	user.find({email : email}, function(err, users){
		console.log("Finding document for email "+email);
		
		if(users.length != 0) {
			user.findOne({email : email}, function(err, doc) {
				//Updating the database of cared for the fence details set by caregiver
				gcm_id_of_cared = doc._id;
				console.log("gcmId is= "+gcm_id_of_cared+" or "+doc._id);
				doc.monitored_by = gcm_id;
				var fence_doc = doc.fence;
				fence_doc.fence_name = fence_name;
				fence_doc.monitored_on = true,
				fence_doc.latitude = latitude,
				fence_doc.longitude = longitude,
				fence_doc.radius = radius,
				doc.save();
				//callback({'response':'Fence updated.', 'res': true});
				
				console.log("Updated in server database");
				
				//Updating caregiver details that its monitoring given cared
				var monitoring = {
					'_id': gcm_id_of_cared,
					'request_status' : true
				};
				user.update(
					{'_id' : gcm_id},
					{'$set' : {'monitoring' : monitoring} 
					},
					function(err, num_affected){
						if(err){
							console.log("Error in update , numbers affected = " +num_affected);
						} else {
							console.log("updated Caregiver that its monitoring cared the requested cared, numbers affected = " +num_affected);
						}
					}
				);
				
					//Notifying cared devices
					//Message
					var message = new gcm.Message({
						collapseKey : 'gcm',
						priority : 'high',
						contentAvailable  : true,
						delayWhileIdle : true,
						timeToLive : 13,
						data : {
							'title' : 'Update Geofence',
							'message' : 'Updating geofence locations',
							'is_update' : "YES",
							'latitude' : latitude,
							'longitude' : longitude,
							'radius' : radius,
							'caregiver_gcmid' : gcm_id,
							'msgcnt':'1'
						},
						notification : {
							title : 'Update geofence',
							body : 'Updating geofence locations'
						}
					});
				
				//Sending the fence update notification to cared device	
				gcmIds.push(gcm_id_of_cared);
				console.log("Id = "+gcm_id_of_cared+"In array = "+gcmIds);
				sender.send(message, gcmIds , 3, function(err, result) {
				console.log("Sending the message to ...= "+gcmIds);
				if(err){
					console.log("Some error in sending");
					console.error(err);
				} 
					else console.log(result);
				});
				
			});
			
		} else {
			console.log("Could not Update in server database");
				//callback({'response':'User not found.', 'res': false});
		}
	});
	callback({'response':"Success", 'res':true });
}