var crypto = require('crypto');
var rand = require('csprng');
var user = require('../model/models');

/* This module is serving API for registration of devices
 * It takes email, password and GCM id of the device for registration, and store in users collection of mongoDb database 
 * The password in encrypted
 * Unique GcmId is used for primary key is users collection
 */
exports.register = function(email, password, gcm_id, callback) {
	var mail_check = email;
	console.log(gcm_id);
	if( gcm_id.length > 10 ) {
		if( !(mail_check.indexOf("@")==mail_check.length) ){
			//Password should contain char and number of length > 4
			if( password.length > 4 ) {
				var temp =rand(160, 36);
				var newpass = temp + password;
				var token = crypto.createHash('sha512').update(email + rand).digest("hex");
				var hashed_password = crypto.createHash('sha512').update(newpass).digest("hex");
				 
				var newuser = new user({
					_id: gcm_id,
					token: token,
					email: email,
					hashed_password: hashed_password,
					salt :temp,
					});
				 
				user.find({_id : gcm_id}, function(err, users_device){
					var len_device = users_device.length;
					console.log(gcm_id+""+len_device);
					if(len_device == 0){
						user.find({email: email}, function(err, users_email){
							var len_email = users_email.length;
							console.log(email+""+len_email);
							if(len_email == 0) {
								newuser.save(function (err) {
									callback({'response':"Device successfully registered", 'res':true });
								});
							}
						});
						
					}else{
						callback({'response':"Device already Registered", 'res':false});
				}});
			}else{
				callback({'response':"Password Weak", 'res' : false });
			}
		} else {
			callback({'response':"Email Not Valid", 'res' : false });
		}
	} else {
		callback({'response': 'Invalid GCM ID', 'res' : false});
	}
}