var mongoose = require("mongoose");
var user = require('../model/models');

exports.monitor= function(gcm_id, email, callback){
	console.log("1. Sent="+email+" " +gcm_id);

	//Updating caregiver details
	var monitoring = {
		'_id': gcm_id,
		'request_status' : true
	};
	user.update(
		{'_id' : gcm_id},
		{'$set' : {	'monitoring' : monitoring} 
		},
		function(err, num_affected){
			if(err){
				console.log("Error in update , numbers affected = " +num_affected);
				callback({'response':'error in monitoring.', 'res': false});
			} else {
				console.log("updated , numbers affected = " +num_affected);
				callback({'response':'Monitoring Confirmed.', 'res': false});
			}
		}
	);
}