var register = require('../controller/register');
var login = require('../controller/login');
var notify = require('../controller/notify');
var setgeofence = require('../controller/setgeofence');
var monitor = require('../controller/monitor');

/* 
 * This module serves routes to the server for incoming REST requests, to provide the intended API's
 */

module.exports = function(app) {  
    app.get('/', function(req, res) {
        res.end("Welcome to Virtualfence app server!");
    });
 
	//Register device using emailId and password
	app.post('/register',function(req,res){
        var email = req.body.email;
        var password = req.body.password;
		var gcm_id = req.body.gcmId;
		
        register.register(email,password, gcm_id, function (found) {
            console.log(found);
            res.json(found);
		});
    });
	
	//Login to the server
    app.post('/login',function(req,res){
        var email = req.body.email;
        var password = req.body.password;
 
        login.login(email,password,function (found) {
            console.log(found);
            res.json(found);
		});
    });
	
	//Update geofence parameters
	app.post('/setgeofence',function(req,res){
		var gcm_id = req.body.gcm_id;
        var email = req.body.email;
		var fence_name = req.body.fence_name;
        var latitude = req.body.latitude;
		var longitude = req.body.longitude;
		var radius = req.body.radius;
        setgeofence.setgeofence(gcm_id, email, fence_name, latitude, longitude, radius, function (found) {
            console.log(found);
            res.json(found);
		});
    });
	
	//Request geofence monitoring
	app.post('/monitor',function(req,res){
		var gcm_id = req.body.gcm_id;
        var email = req.body.email;
        monitor.monitor(gcm_id, email, function (found) {
            console.log(found);
            res.json(found);
		});
    });
 
	//GCM notification
	app.post('/notify', function(req, res) {
		var gcm_id = req.body.gcmId;
		var title = req.body.title;
		notify.notify( gcm_id, title, function(found) {
			console.log(found);
			res.json(found);
		});
	});
 
};


